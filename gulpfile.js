"use strict";

// Load plugins
const autoprefixer = require("autoprefixer");
const gulp = require("gulp");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const sourcemaps = require('gulp-sourcemaps');
const purgecss = require('gulp-purgecss');
const rename = require("gulp-rename");
// CSS task
function css() {
  return gulp
    .src("./scss/**/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest("./css/"))
}
function gulp_purgecss() {
  return gulp
    .src("./css/style.css")
    .pipe(purgecss({content: ['*.html']}))
    .pipe(rename({
      suffix: ".min",
      extname: ".css"
    }))
    .pipe(gulp.dest("./css/"))
}


// Watch files
function watchFiles() {
  gulp.watch("./scss/**/*", css);
  gulp.watch("./scss/**/*", gulp_purgecss);
  gulp.watch("*.html", gulp_purgecss);
}

// define complex tasks
const build = gulp.series(css, gulp_purgecss, watchFiles);

// export tasks
exports.default = build;
