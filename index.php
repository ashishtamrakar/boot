<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/build/style.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <section class="m-t-15">

    <div class="container bg-primary text-white"><div class="row justify-content-center">
      <div class="col-xxl-auto f-12 f-lg-20 wt-lg-300">
        <div class="p-10 p-lg-50"><h1 class="f-20 f-lg-50 wt-400 wt-lg-700">Boot quick bootstrap html/scss starter template</h1>
        <p>Hello world! This is HTML5 Boilerplate.</p></div></div>
    </div></div>
  </section>

  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="js/vendor/jquery-3.3.1.min.js"></script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>

</html>
